<?php
/**
 * @file
 * opr2_feature_base.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function opr2_feature_base_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_opr_data|node|inpatient|form';
  $field_group->group_name = 'group_opr_data';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'inpatient';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'OPR adatok',
    'weight' => '2',
    'children' => array(
      0 => 'field_institute',
      1 => 'field_opr_doctor_code',
      2 => 'field_blood_code',
      3 => 'field_blood_date',
      4 => 'field_hospital',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'OPR adatok',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-opr-data field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_opr_data|node|inpatient|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_opr_profile|user|user|form';
  $field_group->group_name = 'group_opr_profile';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'OPR',
    'weight' => '1',
    'children' => array(
      0 => 'field_doctor_id',
      1 => 'field_institute',
      2 => 'field_country',
      3 => 'field_associated_doctors',
      4 => 'field_associated_study',
      5 => 'field_hospital',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-opr-profile field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_opr_profile|user|user|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_personal|node|inpatient|form';
  $field_group->group_name = 'group_personal';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'inpatient';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => '1. Személyes adatok',
    'weight' => '3',
    'children' => array(
      0 => 'field_patient',
      1 => 'field_inpatient_start',
      2 => 'field_inpatient_end',
      3 => 'field_oldregister_id',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => '1. Személyes adatok',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-opr-data-left group-inline-fields',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_personal|node|inpatient|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('1. Személyes adatok');
  t('OPR');
  t('OPR adatok');

  return $field_groups;
}
