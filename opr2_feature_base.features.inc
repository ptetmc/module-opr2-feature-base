<?php
/**
 * @file
 * opr2_feature_base.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function opr2_feature_base_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function opr2_feature_base_node_info() {
  $items = array(
    'hospital' => array(
      'name' => t('Kórház'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Név'),
      'help' => '',
    ),
    'inpatient' => array(
      'name' => t('Befekvés'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'patient' => array(
      'name' => t('Beteg'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
